package com.kenfogel.priorityqueue;

import java.util.PriorityQueue;
import java.util.Queue;

/**
 * Demonstration of a priority queue
 * 
 * @author Ken Fogel
 */
public class PriorityQueueExample {

    // Length of priority queue
    private static final int QSIZE = 3;
    
    /**
     * Demonstrate the priority queue
     */
    public void perform() {
        
        AgeComparator ac = new AgeComparator();
        Queue<Person> personPriorityQueue = new PriorityQueue<>(QSIZE, ac);
        
        personPriorityQueue.add(new Person("Joe", 24));
        personPriorityQueue.add(new Person("Pete", 18));
        personPriorityQueue.add(new Person("Chris", 21));
        
        for(int x = 0; x < QSIZE; ++x) {
            System.out.println(personPriorityQueue.poll());
        }
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        PriorityQueueExample pqe = new PriorityQueueExample();
        pqe.perform();
    }

}
