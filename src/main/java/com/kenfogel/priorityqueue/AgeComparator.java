package com.kenfogel.priorityqueue;

import java.util.Comparator;

/**
 * Comparator class for the priority queue demo
 * 
 * @author Ken Fogel
 */
public class AgeComparator implements Comparator<Person> {
    @Override
    public int compare(Person a, Person b) {
        return a.age < b.age ? -1 : a.age == b.age ? 0 : 1;
    }
}
