package com.kenfogel.priorityqueue;

/**
 * Class to enter into a priority queue
 * 
 * @author Ken Fogel
 */
public class Person {

    String name;
    int age;

    Person(String n, int a) {
        name = n;
        age = a;
    }

    @Override
    public String toString() {
        return String.format("{name=%s, age=%d}", name, age);
    }
}
